#!/usr/bin/env python
import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pychip",
    version="0.0.5",
    author='Sarah Akers',
    author_email="sarah.akers@pnnl.gov",
    description="A package for few-shot segmentation of high resolution images",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.pnnl.gov/reeh135/pychip",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'cycler==0.10.0',
        'kiwisolver==1.3.1',
        'matplotlib==3.3.4',
        'numpy==1.19.4',
        'opencv-python==4.5.1.48',
        'pandas==1.2.0',
        'Pillow==8.0.1',
        'pyparsing==2.4.7',
        'python-dateutil==2.8.1',
        'pytz==2020.5',
        'six==1.15.0',
        'torch==1.7.1',
        'torchvision==0.8.2',
        'typing-extensions==3.7.4.3'

    ]
)
