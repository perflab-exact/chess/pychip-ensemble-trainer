#!/bin/bash
#SBATCH --job-name=jupyter
#SBATCH --time=2-00:00:00
#SBATCH --output=jupyter.log

module load python/3.6.6 #miniconda3.6
#source activate /share/apps/python/miniconda3.6
#pip install --user jinja2
#pip install --user prometheus_client
#pip install --user nbformat
#pip install --user send2trash
#pip install --user websocket
#cat /etc/hosts
jupyter notebook --ip=0.0.0.0 --port=8888
