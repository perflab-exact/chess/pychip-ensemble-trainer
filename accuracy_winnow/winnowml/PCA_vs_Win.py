import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 18})
from get_input import input_data
from get_output import output_data
from MLE2 import MLE
import numpy as np
import statistics
import math
from scipy.spatial import distance
from sklearn.decomposition import PCA
import statistics
from scipy.stats import kurtosis
from scipy.stats import skew

def get_relative_error(pred, targ):
    # This function will calculate the relative error between target and predicted values
    RE = []
    for i in range(len(pred)):
        if targ[i] == 0:
            if pred[i] > targ[i]:
                RE.append(abs(pred[i]-targ[i]))
            else:
                RE.append(abs(targ[i]-pred[i]))
        else:
            if pred[i] > targ[i]:
                RE.append((abs(pred[i]-targ[i]))/targ[i])
            else:
                RE.append((abs(targ[i]-pred[i]))/targ[i])
    return RE

in_ = input_data()
out_ = output_data()
mle = MLE()
data, feature_list = in_.get_input_features('../list_maker/data/20191019.csv', 95000, ID_=(23,"eos/draining")) #sc-extract-cms-2019-05.csv 95000 out.csv
i = 0
target_vals = in_.get_target(data)#, feature_list[i])
target_IDs = ["Throughput"]#feature_list[i]
t_feature_list = list(feature_list.copy())
target_vals = np.array(target_vals)
target_vals = target_vals.transpose()
target_vals = target_vals.reshape((target_vals.shape[0], 1,target_vals.shape[1]))
comb_features, bool_d = in_.organize_feature(data, target_vals, t_feature_list, mle)


out = out_.get_pred(data, target_vals, comb_features, mle, 10, bool_d)
metrics, rr = out_.sel_pred(out, target_vals[int(round(len(target_vals)*0.8)):], i, bool_d)
A = out_.set_weights(metrics, 30)#15
resultingoccurrence = out_.get_ind(metrics,A)
selected_comb, dir_ = out_.sel_high_comb(resultingoccurrence, comb_features, rr)
             
mle.model = None
training_vals = in_.format_data(data, selected_comb, bool_d)
training_vals = np.array(training_vals)
training_vals = training_vals.transpose()
training_vals = training_vals.reshape((training_vals.shape[0], 1, training_vals.shape[1]))
mle.train(training_vals, target_vals, int(round(len(training_vals)*0.8)))
prediction_WinnowML = mle.predict(training_vals[int(round(len(training_vals)*0.8)):])

metrics_PCA = []
for i in range(0,len(t_feature_list)):
    training_vals = in_.format_data(data, t_feature_list, bool_d)
    training_vals = np.array(training_vals)
    training_vals = training_vals.transpose()

    pca = PCA(n_components=i+1)
    training_vals = pca.fit_transform(training_vals)
    print(training_vals)


    mle.model = None
    training_vals = training_vals.reshape((training_vals.shape[0], 1, training_vals.shape[1]))
    mle.train(training_vals, target_vals, int(round(len(training_vals)*0.8)))
    prediction = mle.predict(training_vals[int(round(len(training_vals)*0.8)):])
    prediction = prediction.reshape((prediction.shape[0], prediction.shape[2]))
    target_vals = target_vals.reshape((target_vals.shape[0], target_vals.shape[2]))

    print(prediction.shape)
    print(target_vals.shape)

    AE = get_relative_error(prediction[:,0], target_vals[int(round(len(training_vals)*0.8)):,0])
    #print(AE)
    kurt1 = kurtosis(target_vals[:, 0])
    kurt2 = kurtosis(prediction[:, 0])
    if kurt1 > kurt2:
        kurt = kurt1 - kurt2
    else:
        kurt = kurt2 - kurt1
    skew1 = skew(target_vals[:, 0])
    skew2 = skew(prediction[:, 0])
    if skew1 > skew2:
        skew3 = skew1 - skew2
    else:
        skew3 = skew2 - skew1
    std = statistics.stdev(AE)
    metrics_PCA.append([sum(AE)/len(AE), std, kurt, skew3])
    target_vals = target_vals.reshape((target_vals.shape[0], 1,target_vals.shape[1]))

print("========================================")
metrics_ = []
for i in range(len(metrics)):
    metrics_.append(metrics[i].transpose())
metrics = np.array(metrics_)
metrics = metrics.reshape((metrics.shape[0], metrics.shape[1]))
print(metrics.shape)
print("******************************")
metrics_PCA = np.array(metrics_PCA)
print(metrics_PCA.shape)
#print(prediction_WinnowML.shape)
#prediction_WinnowML = prediction_WinnowML.reshape((prediction_WinnowML.shape[0], prediction_WinnowML.shape[1]))
#AE_W = get_relative_error(prediction_WinnowML[:, 0], target_vals[int(round(len(training_vals)*0.8)):,0])
#std_W = statistics.stdev(AE_W)
#print(sum(AE_W)/len(AE_W))
#print(std_W)
#print(len(selected_comb))
fig = plt.figure()
fig.set_figheight(12)
fig.set_figwidth(12)

metrics_ = []
for i in range(len(metrics_PCA)):
    tt = metrics_PCA[i].reshape((len(metrics_PCA[i]), 1))
    metrics_.append(tt.transpose())
metrics_PCA = np.array(metrics_)


x = range(1,len(metrics)+1)
A = out_.set_weights(metrics_PCA, 30)#15
ID_PCA = out_.get_ind(metrics_PCA,A)
max_ = max(ID_PCA)
ind_PCA = ID_PCA.index(max_) + 1

metrics_ = []
for i in range(len(metrics_PCA)):
    metrics_.append(metrics_PCA[i].transpose())
metrics_PCA = np.array(metrics_)
metrics_PCA = metrics_PCA.reshape((metrics_PCA.shape[0], metrics_PCA.shape[1]))

print("selected by WinnowML")
print(len(selected_comb))
print("selected by PCA")
print(ind_PCA)

plt.subplot(1, 1, 1)
plt.errorbar(x, metrics[:, 0], yerr=metrics[:, 1], fmt='o', label="WinnowML")
plt.scatter(len(selected_comb), metrics[len(selected_comb)-1, 0], s=80,color="g", marker='X', label="Selected by WinnowML")
plt.errorbar(x, metrics_PCA[:, 0], yerr=metrics_PCA[:, 1], fmt='o', label="PCA")
#plt.scatter(ind_PCA, metrics_PCA[ind_PCA-1, 0], s=80, color="r", marker='X', label="selected by PCA")
plt.legend(loc="upper center", ncol=len(x), mode="expand", borderaxespad=0.)
plt.title('Accuracy metrics vs Number of Features')
plt.ylabel('Mean Absolute Error (MAE)')
x_t = np.arange(1, len(x)+1, 5)
plt.xticks(x_t)
plt.tight_layout()
fig.savefig("PCA_v_Winnow.png")
