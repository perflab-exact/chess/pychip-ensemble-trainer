#This file contains the neural network that will map the training features to the target values
import os
import numpy as np
np.random.seed(3900) #5400
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"
import keras
import time
import math
from keras.models import Model, load_model
from keras.models import Sequential
from keras.layers import Input, GaussianNoise, Embedding, BatchNormalization
from keras.layers import Dense, Dropout, Activation, Flatten, TimeDistributed
from keras.layers import Conv2D,Conv1D, MaxPooling1D, LeakyReLU, PReLU
from keras.utils import np_utils
from keras.callbacks import EarlyStopping
from keras.callbacks import CSVLogger, ModelCheckpoint
from keras.regularizers import l2
import h5py
from keras.layers import LSTM, Reshape,  Bidirectional, GRU, SimpleRNN
import tensorflow as tf
from tensorflow.python.client import device_lib
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from keras.backend.tensorflow_backend import set_session
from keras import backend as K
from math import sqrt
from keras.optimizers import RMSprop, Adam, SGD

class MLE:
    model = None

    def create_model2D(self, step_size, nb_features):
        inputs = Input(shape=(step_size,))
        preds = Dense(71, activation='elu')(inputs)
        preds = Reshape((71, 1))(preds)
        preds = GRU(106, return_sequences=True)(preds)
        preds = Reshape((71*106, ))(preds)
        preds = Dense(1, activation='relu')(preds)
        '''
        preds = Dense(618, activation='relu')(inputs)
        #preds = Dense(242, activation='linear')(preds)
        #print(preds.shape)
        preds = Reshape((618, 1))(preds)
        preds = Conv1D(filters=242, kernel_size=1)(preds)
        #print(preds.shape)
        preds = Reshape((242*618, ))(preds)
        preds = Dense(78, activation='relu')(preds)
        preds = Dense(1, activation='linear')(preds)
        '''
        """
        preds = Dense(step_size, activation='relu')(inputs)
        #preds = Dense(step_size, activation='relu')(preds)
        #preds = Dense(step_size, activation='relu')(preds)
        #preds = Dense(step_size*16, activation='relu')(preds)
        #preds = Reshape((1, step_size))(inputs)
        #preds = LSTM(step_size, return_sequences=True)(preds)
        #preds = LSTM(step_size, return_sequences=True)(preds)
        #preds = GRU(step_size, return_sequences=True)(preds)
        #preds = GRU(step_size, return_sequences=True)(preds)
        #preds = GRU(step_size, return_sequences=True)(preds)
        #preds = SimpleRNN(step_size, return_sequences=True)(preds)
        #preds = Reshape((step_size, ))(preds)
        #preds = Dense(step_size, activation='relu')(preds)
        #preds = Dense(nb_features, activation='relu')(preds)
        preds = Dense(nb_features)(preds)
        model = Model(inputs=inputs,outputs=preds)
        #############GEOMANCY TEST###########################
        #inputs = Input(shape=(step_size,))
        #preds = Dense(step_size*16, activation='relu')(inputs)
        #preds = Dense(step_size*8, activation='relu')(preds)
        #preds = Dense(step_size*4, activation='relu')(preds)
        #preds = Dense(step_size*16, activation='relu')(preds)
        #preds = Dense(1,activation='linear')(preds)
        """
        model = Model(inputs=inputs,outputs=preds)
        return model


    def create_model(self, step_size, nb_features):
        #This function will create the neural network model
        #Might need to take as input the corresponding training set to set the size
        #will return the model object
        inputs = Input(shape=(nb_features,step_size))
        preds = Dense(71, activation='elu')(inputs)
        preds = GRU(106, return_sequences=True)(preds)    
        preds = Dense(1, activation='relu')(preds)
        '''
        preds = Dense(618, activation='relu')(inputs)
        preds = Conv1D(filters=242, kernel_size=1)(preds)
        preds = Dense(78, activation='relu')(preds)
        preds = Dense(1, activation='linear')(preds)
        '''
        '''
        inputs = Input(shape=(1,step_size))
        preds = Dense(step_size, activation='relu')(inputs)
        #preds = Dense(step_size, activation='relu')(preds)
        #preds = Dense(step_size, activation='relu')(preds)
        #preds = Dense(step_size*16, activation='relu')(preds)
        #preds = SimpleRNN(step_size, return_sequences=True)(inputs)
        #preds = LSTM(step_size, return_sequences=True)(inputs)
        #preds = LSTM(step_size, return_sequences=True)(preds)
        #preds = GRU(step_size, return_sequences=True)(preds)
        #preds = GRU(step_size, return_sequences=True)(preds)
        #preds = GRU(step_size, return_sequences=True)(preds)
        #preds = Dense(nb_features, activation='relu')(preds)
        preds = Dense(nb_features)(preds)
        model = Model(inputs=inputs,outputs=preds)
        #inputs = Input(shape=(1, step_size))
        #preds = Dense(step_size*16, activation='relu')(inputs)
        #preds = Dense(step_size*8, activation='relu')(preds)
        #preds = Dense(step_size*4, activation='relu')(preds)
        #preds = Dense(step_size*16, activation='relu')(preds)
        #preds = Dense(1,activation='linear')(preds)
        '''
        model = Model(inputs=inputs,outputs=preds)
        return model

    def train(self, X, Y, training_test_split):
        #This function will trigger the training of the neural network object
        #X is the input training dataset
        #Y are the target values
        #training_test_split is the split between the training and testing sets (60:20:20)
        step_size = X.shape[len(X.shape)-1] #number of input feature columns
        nb_features = Y.shape[len(Y.shape)-1] #number of target features

        print(step_size)
        print(nb_features)
        print("99999999999999999999999999999")
        if self.model == None:
            if len(X.shape) == 2:
                self.model = self.create_model2D(step_size, nb_features)
            else:
                self.model = self.create_model(step_size, nb_features)
            rms=Adam(learning_rate=0.0001)#SGD() #Standard gradient descent (keep things simple)
            self.model.compile(loss='mse', optimizer=rms, metrics=['mse']) #Mean square error would allow us to determine how far the prediction is from the target values

            
        es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=50)
        hist = self.model.fit(X[:training_test_split], Y[:training_test_split], batch_size=51, epochs=151, verbose=0, validation_split=0.25)#, callbacks=[es]) #Still need to identify the hyperperamters for the training such as batch_size, epochs
        return self.model

    def predict(self, X_test):
        #This function will use X_test to predict the target values
        #It will return the target values to be used for comparison with actual target values
        predicted = self.model.predict(X_test)
        return predicted
