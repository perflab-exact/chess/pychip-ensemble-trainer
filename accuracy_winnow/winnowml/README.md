# WinnowML
This repo contains only the code.

When seting up WinnowML make sure the code points to the correct location of the data in the get\_input\_features()
ID\_=(23,"fuse") in the get\_input\_features() function is the command used to indicate to WinnowML which value to use in a column


To run WinnowML only: python3 time\_base\_vs\_Win\_only\_win.py 
To run WinnowML with all the base case: python3 time\_base\_vs\_Win.py
For this experiment, we used a updated version of pystreamfs library where we changed the classify function in the Utils class to use the mean absolute error between the prediction and target values. Additionally, in the same piece of code we updated the model training to match the one set in the model class give by the user. This was done to provide a fair comparison between the approaches
This experiment without a GPU will take 2.5 days to run

make sure to have a perf\_data folder created (this is where WinnowML will output the resulting accuracy)

## User input
To use WinnowML a user will have to provide the following elements:


1. The dataset
2. The location of the target value in the dataset (We are currently experimenting with ways to have the target value be calculated by WinnowML but that is just working for throughput currently)
3. MLE.py (MLE2.py for time\_base\_vs\_Win\_only\_win.py and time\_base\_vs\_Win.py)

## Data outputed
mean\_RE0 and std\_RE0 are the files that contain the mean and standard deviation of the absolute relative error between the prediction and target values

## Current state of the code
Currently this code base works with CERN and Backblaze datasets
It only works for timeseries prediction
We are starting to work on classification problems

## Warnings
When running any python script in this directory make sure that the file is correctly pointing to where the data is located at.
Please ignore kurt0 and skew0 they were supposed to be used for the kurtosis and skew values but they are no longer being used by WinnowML.
