#This file contains the neural network that will map the training features to the target values
import os
import numpy as np
np.random.seed(3900) #5400
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"
import time
import math
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Input, GaussianNoise, Embedding, BatchNormalization
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, TimeDistributed
from tensorflow.keras.layers import Conv2D,Conv1D, MaxPooling1D, LeakyReLU, PReLU
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.callbacks import CSVLogger, ModelCheckpoint
from tensorflow.keras.regularizers import l2
import h5py
from tensorflow.keras.layers import LSTM, Reshape,  Bidirectional, GRU, SimpleRNN
import tensorflow as tf
from tensorflow.python.client import device_lib
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from tensorflow.keras import backend as K
from math import sqrt
from tensorflow.keras.optimizers import RMSprop, Adam, SGD

class MLE:
    model = None

    def create_model2D(self, step_size, nb_features):
        inputs = Input(shape=(step_size,))

        
        ###Geomancy multiple mounts
        preds = Dense(step_size, activation='elu')(inputs)
        preds = Reshape((step_size, 1))(preds)
        preds = LSTM(step_size, return_sequences=True)(preds)
        preds = Reshape((step_size*step_size, ))(preds)
        preds = Dense(1, activation='relu')(preds)
        

        '''
        ###SpeedRacer multiple mounts
        preds = Dense(step_size, activation='relu')(inputs)
        preds = Dense(step_size, activation='relu')(preds)
        preds = Dense(616, activation='elu')(preds)
        preds = Dense(1, activation='relu')(preds)
        '''

        """
        ###SpeedRacer 1 mount
        preds = Dense(step_size, activation='relu')(inputs)
        preds = Reshape((step_size, 1))(preds)
        preds = SimpleRNN(step_size, return_sequences=True)(preds)
        preds = Reshape((step_size*step_size, ))(preds)
        preds = Dense(308, activation='relu')(preds)
        preds = Dense(1, activation='relu')(preds)
        """
        
        
        '''
        ###Geomancy 1 mount
        preds = Reshape((step_size, 1))(inputs)
        preds = SimpleRNN(14, return_sequences=True)(preds)
        preds = SimpleRNN(14, return_sequences=True)(preds)
        preds = Reshape((step_size*step_size, ))(preds)
        preds = Dense(126, activation='relu')(preds)
        preds = Dense(1, activation='linear')(preds)
        '''
        #preds = Dense(77, activation='relu')(inputs)
        #preds = Dense(1, activation='relu')(preds)
        
        """
        preds = Dense(step_size, activation='relu')(inputs)
        #preds = Dense(step_size, activation='relu')(preds)
        #preds = Dense(step_size, activation='relu')(preds)
        #preds = Dense(step_size*16, activation='relu')(preds)
        #preds = Reshape((1, step_size))(inputs)
        #preds = LSTM(step_size, return_sequences=True)(preds)
        #preds = LSTM(step_size, return_sequences=True)(preds)
        #preds = GRU(step_size, return_sequences=True)(preds)
        #preds = GRU(step_size, return_sequences=True)(preds)
        #preds = GRU(step_size, return_sequences=True)(preds)
        #preds = SimpleRNN(step_size, return_sequences=True)(preds)
        #preds = Reshape((step_size, ))(preds)
        #preds = Dense(step_size, activation='relu')(preds)
        #preds = Dense(nb_features, activation='relu')(preds)
        preds = Dense(nb_features)(preds)
        model = Model(inputs=inputs,outputs=preds)
        #############GEOMANCY TEST###########################
        #inputs = Input(shape=(step_size,))
        #preds = Dense(step_size*16, activation='relu')(inputs)
        #preds = Dense(step_size*8, activation='relu')(preds)
        #preds = Dense(step_size*4, activation='relu')(preds)
        #preds = Dense(step_size*16, activation='relu')(preds)
        #preds = Dense(1,activation='linear')(preds)
        """
        model = Model(inputs=inputs,outputs=preds)
        return model


    def create_model(self, step_size, nb_features):
        #This function will create the neural network model
        #Might need to take as input the corresponding training set to set the size
        #will return the model object
        inputs = Input(shape=(nb_features,step_size))
        
        ###Geomancy multiple mounts
        preds = Dense(step_size, activation='elu')(inputs)
        preds = LSTM(step_size, return_sequences=True)(preds)
        preds = Dense(1, activation='relu')(preds)

        """ 
        ###SpeedRacer Multiple mounts
        preds = Dense(step_size, activation='relu')(inputs)
        preds = Dense(step_size, activation='relu')(preds)
        preds = Dense(616, activation='elu')(preds)
        preds = Dense(1, activation='relu')(preds)
        """
        '''
        ####SpeedRacer 1 mount
        preds = Dense(step_size, activation='relu')(inputs)
        preds = SimpleRNN(step_size, return_sequences=True)(preds)
        preds = Dense(308, activation='relu')(preds)
        preds = Dense(1, activation='relu')(preds)
        '''
        '''
        ####Geomancy 1 mount
        preds = SimpleRNN(14, return_sequences=True)(inputs)
        preds = SimpleRNN(14, return_sequences=True)(preds)
        preds = Dense(126, activation='relu')(preds)
        preds = Dense(1, activation='linear')(preds)
        '''
        #preds = Dense(77, activation='relu')(inputs)
        #preds = Dense(1, activation='relu')(preds)
        
        '''
        inputs = Input(shape=(1,step_size))
        preds = Dense(step_size, activation='relu')(inputs)
        #preds = Dense(step_size, activation='relu')(preds)
        #preds = Dense(step_size, activation='relu')(preds)
        #preds = Dense(step_size*16, activation='relu')(preds)
        #preds = SimpleRNN(step_size, return_sequences=True)(inputs)
        #preds = LSTM(step_size, return_sequences=True)(inputs)
        #preds = LSTM(step_size, return_sequences=True)(preds)
        #preds = GRU(step_size, return_sequences=True)(preds)
        #preds = GRU(step_size, return_sequences=True)(preds)
        #preds = GRU(step_size, return_sequences=True)(preds)
        #preds = Dense(nb_features, activation='relu')(preds)
        preds = Dense(nb_features)(preds)
        model = Model(inputs=inputs,outputs=preds)
        #inputs = Input(shape=(1, step_size))
        #preds = Dense(step_size*16, activation='relu')(inputs)
        #preds = Dense(step_size*8, activation='relu')(preds)
        #preds = Dense(step_size*4, activation='relu')(preds)
        #preds = Dense(step_size*16, activation='relu')(preds)
        #preds = Dense(1,activation='linear')(preds)
        '''
        model = Model(inputs=inputs,outputs=preds)
        return model

    def train(self, X, Y, training_test_split):
        #This function will trigger the training of the neural network object
        #X is the input training dataset
        #Y are the target values
        #training_test_split is the split between the training and testing sets (60:20:20)
        step_size = X.shape[len(X.shape)-1] #number of input feature columns
        nb_features = Y.shape[len(Y.shape)-1] #number of target features

        print(step_size)
        print(nb_features)
        print(X.shape)
        print(Y.shape)
        print("99999999999999999999999999999")
        if self.model == None:
            if len(X.shape) == 2:
                self.model = self.create_model2D(step_size, nb_features)
            else:
                self.model = self.create_model(step_size, nb_features)
            rms=Adam(learning_rate=0.01)#(changed 0.01 to 0.0001 for speedRacer multiple mount)SGD() #Standard gradient descent (keep things simple)
            self.model.compile(loss='mae', optimizer=rms, metrics=['mae']) # Mean square error would allow us to determine how far the prediction is from the target values

            
        es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=50)
        hist = self.model.fit(X[:training_test_split], Y[:training_test_split], batch_size=10, epochs=291, verbose=0, validation_split=0.25, callbacks=[es]) #(changed 51 to 291 for Geomancy multiple mount and 291 to 221 for speedracer multiple mounts) Still need to identify the hyperperamters for the training such as batch_size, epochs
        return self.model

    def predict(self, X_test):
        #This function will use X_test to predict the target values
        #It will return the target values to be used for comparison with actual target values
        predicted = self.model.predict(X_test)
        return predicted
