import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from get_input import input_data
from get_output import output_data
from MLE2 import MLE
import numpy as np
import statistics
import math
from scipy.spatial import distance

f = open("../timing.txt", "r")
content = f.read()
f.close
content = content.split("\n")
training = []
prediction = []

for i in range(len(content)-1):
    temp = content[i].split(",")
    training.append(float(temp[0]))
    prediction.append(float(temp[1]))

fig = plt.figure()
fig.set_figheight(4)
fig.set_figwidth(4)
iplot = 1
label = ["training"]#, "prediction"]
for i in range(len(label)):#chosen_ports)):
    plt.subplot(len(label), 1,iplot)
    xs = np.arange(len(training))
    t = training
    if i == 1:
        t = prediction
    plt.plot(xs, t, lw=1)
    plt.title("Time vs number of features\nused for " + label[i])
    plt.xlabel("Number of feature used")
    plt.ylabel("Latency of "+label[i]+" in s")
    iplot += 1
plt.tight_layout()
fig.savefig("time_analysis.png")
