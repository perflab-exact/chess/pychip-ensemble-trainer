#This file will be used to select from the predicted target values the selected subset of features to be used for predicting the target values
import numpy as np
#np.random.seed(3900)
from accuracy_winnow.get_input import input_data
import statistics 
import os
import time
import math
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import average_precision_score
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import accuracy_score
import sklearn.metrics as mtp
from scipy.stats import kurtosis
from scipy.stats import skew
from keras import backend as K 

class output_data:
    def Nmaxelements(self, list1, N, min_max): 
        final_list = [] 
        list1 = list(list1)      
        for i in range(0, N):  
            if min_max in "min":
                max1 = min(list1)
            elif min_max in "max":
                max1 = max(list1)
                                        
            #for j in range(len(list1)):      
            #    if list1[j] > max1 and min_max in "max": 
            #        max1 = list1[j]
            #    elif list1[j] < max1 and min_max in "min":
            #        max1 = list1[j]
                                                                                                      
            list1.remove(max1); 
            final_list.append(max1)
        return final_list
                                                                                                                                
                                                                                                                       
    def get_relative_error(self, pred, targ):
        # This function will calculate the relative error between target and predicted values
        RE = []
        print(pred)
        print(targ)
        for i in range(len(pred)):
            if pred[i] <= 0:
                if pred[i] > targ[i]:
                    RE.append(abs(pred[i]-targ[i]))
                else:
                    RE.append(abs(targ[i]-pred[i]))
            else:
                if pred[i] > targ[i]:
                    RE.append((abs(pred[i]-targ[i]))/pred[i])
                else:
                    RE.append((abs(targ[i]-pred[i]))/pred[i])
        #print(RE)
        return RE

    def get_direction(self, pred, targ):
        RE = []
        for i in range(len(pred)):
            if targ[i] == 0:
                RE.append(targ[i]-pred[i])
            else:
                RE.append(targ[i]-pred[i]/pred[i])
        m_RE = sum(RE)/len(RE)
        return m_RE

    def get_pred(self, data, target_vals, comb_features, mle, num_comb, bool_d, smooth=None):
        #X is the input performance features
        #Y is the selected target values
        #This function will group all the predictions into one dataset
        in_ = input_data()
        out = {}
        print(len(comb_features))
        #removes past timing data
        if os.path.isfile('timing.txt'):
            os.remove('timing.txt')
        f= open("timing.txt","a")
        print(len(comb_features))
        for i in range(len(comb_features)):
            mle.model = None
            #K.clear_session()
            training_vals = in_.format_data(data, comb_features[i], bool_d, smooth=smooth)
            training_vals = np.array(training_vals)
            training_vals = training_vals.transpose()
            if len(target_vals.shape) == 3:
                training_vals = training_vals.reshape((training_vals.shape[0], 1, training_vals.shape[1]))
            start = time.time()
            mle.train(training_vals, target_vals, int(round(len(training_vals)*0.8)))
            end = time.time()
            prediction = mle.predict(training_vals[int(round(len(training_vals)*0.8)):])
            end_p = time.time()
            time1 = end - start
            time2 = end_p - end
            f.write(str(time1)+","+str(time2)+"\n")
            print(prediction.shape)
            #print(i)
            if len(prediction.shape) == 1:
                prediction = prediction.reshape((prediction.shape[0], 1))
            #print(prediction)
            #if len(prediction.shape) > 2:
            out[i] = prediction.reshape((prediction.shape[0], prediction.shape[len(prediction.shape)-1]))
            K.clear_session()
        f.close()
        return out

    def sel_pred(self, out, target, ind, bool_d):
        #Run predictions and target values through the following metrics:
        #correlation
        #mean relative error
        #standard deviation of mean relative error
        #euclidean or manhattan features
        #From the output of the previous metrics select the best subset
        #print(target.shape)
        #print(out)
        #if len(target.shape) == 3:
        target = target.reshape((target.shape[0], target.shape[len(target.shape)-1]))
        metrics = []
        relative_ee = []
        f = open("mean_RE"+str(ind), "a")
        f1 = open("std_RE"+str(ind), "a")
        f2 = open("kurt"+str(ind), "a")
        f3 = open("skew"+str(ind), "a")
        for i in out.keys():
            temp_metrics = []
            string_mean = ""
            string_std = ""
            string_kurt = ""
            string_skew = ""
            for j in range(out[i].shape[1]):
                #cor = np.corrcoef(out[i][:, j], target[:, j])[0, 1]
                if bool_d == 0:
                    print(out[i][:, j])
                    print(mtp.classification_report(target[:, j],out[i][:, j]))
                    acc = mtp.precision_score(target[:, j],out[i][:, j], average="macro")
                    mean = 1-acc
                    rec = mtp.recall_score(target[:, j],out[i][:, j], average="macro")
                    std = 1-rec
                    kurt1 = kurtosis(target[:, j])
                    kurt2 = kurtosis(out[i][:, j])
                    if kurt1 > kurt2:
                        kurt = (kurt1 - kurt2)
                    else:
                        kurt = (kurt2 - kurt1)

                    skew1 = skew(target[:, j])
                    skew2 = skew(out[i][:, j])
                    if skew1 > skew2:
                        skew3 = (skew1 - skew2)/skew2
                    else:
                        skew3 = (skew2 - skew1)/skew2
                    temp_metrics.append([mean, std, kurt, skew3])

                else:
                    print(out[i].shape)
                    print(target.shape)
                    RE = self.get_relative_error(out[i][:, j],target[:, j])
                    m_RE = self.get_direction(out[i][:, j],target[:, j])
                    mean = sum(RE)/len(RE)
                    print(sum(RE))
                    print(len(RE))
                    print(np.array(RE))
                    print("MOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO")
                    std = statistics.stdev(RE)
                    kurt1 = kurtosis(target[:, j])
                    kurt2 = kurtosis(out[i][:, j])
                    if kurt1 < 0 and kurt2 < 0:
                        kurt1 -= kurt1
                        kurt2 -= kurt2
                    if kurt1 > kurt2:
                        if kurt2 == 0:
                            kurt = (kurt1 - kurt2)
                        elif kurt2 < 0:
                            kurt = (kurt1 - kurt2)/(-kurt2)
                        else:
                            kurt = (kurt1 - kurt2)/kurt2
                    else:
                        if kurt2 == 0:
                            kurt = (kurt2 - kurt1)
                        elif kurt2 < 0:
                            kurt = (kurt2 - kurt1)/(-kurt2)
                        else:
                            kurt = (kurt2 - kurt1)/kurt2

                    skew1 = skew(target[:, j])
                    skew2 = skew(out[i][:, j])
                    if skew1 < 0 and skew2 < 0:
                        skew1 -= skew1
                        skew2 -= skew2
                    if skew1 > skew2:
                        if skew2 == 0:
                            skew3 = (skew1 - skew2)
                        else:
                            skew3 = (skew1 - skew2)/skew2
                    else:
                        if skew2 == 0:
                            skew3 = (skew2 - skew1)
                        else:
                            skew3 = (skew2 - skew1)/skew2
                    string_mean += str(mean) + ","
                    string_std += str(std) + ","
                    string_kurt += str(kurt)+ ","
                    string_skew += str(skew3)+ ","
                    temp_metrics.append([mean, std, 0, 0]) #kurt
                    relative_ee.append(m_RE)
                    string_mean = string_mean[:-1] + "\n"
                    string_std = string_std[:-1] + "\n"
                    string_kurt = string_kurt[:-1]+ "\n"
                    string_skew = string_skew[:-1]+ "\n"
                    f.write(string_mean)
                    f1.write(string_std)
                    f2.write(string_kurt)
                    f3.write(string_skew)
            metrics.append(temp_metrics)
        f.close()
        f1.close()
        f2.close()
        f3.close()
        metrics = np.array(metrics)
        print("This is the shape of the metrics:")
        print(metrics.shape)
        return (metrics, relative_ee)

    def sel_comb(self, metrics, num):
        max_cor = []
        mean_RE = []
        min_std_RE = []
        for i in range(metrics.shape[1]):
            #max_cor.append(self.Nmaxelements(metrics[:, i, 0], 5, "max"))
            mean_RE.append(self.Nmaxelements(metrics[:, i, 0], metrics.shape[0], "min"))
            min_std_RE.append(self.Nmaxelements(metrics[:, i, 1], metrics.shape[0], "min"))
        out_ = {"mean": mean_RE, "std": min_std_RE}
        return out_

    def set_weights(self, metrics, NT):
        A = [1/4,1/4,1/4,1/4]#1/4,1/4,1/4,1/4]
        mu = 0.25
        c = 1/float(mu)
        alpha = 0.25
        print(metrics)
        t = []
        weights = [1, 1, 1, 1]
        for x in range(NT):
            for i in range(len(metrics)):
                temp = 0
                for j in range(len(metrics[i,0])):
                    temp += weights[j]*metrics[i,0,j]
                t.append(temp)
            loss = []
            for i in range(len(A)):
                A[i] = weights[i]
                resultingoccurrence = self.get_ind(metrics,A)#out_.recurrence(ind_met, len(comb_features))
                temp2 = 0
                for j in range(len(metrics[resultingoccurrence.index(max(resultingoccurrence)),0])):
                    temp2 += weights[j]*metrics[resultingoccurrence.index(max(resultingoccurrence)),0,j]
                loss.append(temp2)# - min(t))
                A = [1/4,1/4,1/4,1/4]
            nw = []
            for i in range(len(loss)):
                nw.append(math.exp( -(mu*loss[i]) )*weights[i])
            pool = 0
            for i in range(len(loss)):
                pool += (1-(1-alpha)**loss[i])*nw[i]
            new_weights = []
            for i in range(len(loss)):
                new_weights.append(((1-alpha)**loss[i])*nw[i] + (1/(len(loss)-1))*(pool - (1-(1-alpha)**loss[i])*nw[i]))
            weights = new_weights

        return weights


    def get_ind(self, metrics, A, wnf=0, thnf=-1, therr=-1):
        ID = []#{"mean":[], "std":[]}
        werr = 1 - wnf
        for i in range(metrics.shape[1]):
            list_RE = list(metrics[:, i, 0])
            list_std = list(metrics[:, i, 1])
            list_kurt = list(metrics[:, i, 2])
            list_skew = list(metrics[:, i, 3])
            for j in range(len(metrics)):#val_met["mean"][i])):
                if thnf != -1 and thnf < (j+1):
                    break
                if (therr != -1 and therr > (list_RE[j]+list_std[j])) or (therr == -1):
                    ID.append(1/(wnf*(j+1)+werr*(float(A[0])*list_RE[j]+float(A[1])*list_std[j] + float(A[2])*list_kurt[j] + float(A[3])*list_skew[j] +1)))
                else:
                    ID.append(0)
        if len(ID) == 0:
            return "Did not find a subgroup that satisfied the threshold. Please update the threshold or the modeling approach. Thanks!"
        else:
            return ID

    def recurrence(self, ID, num_comb):
        found_ID = []
        for i in range(num_comb):
            found_ID.append(0)
        for i in ID.keys():
            for j in ID[i]:
                count = 1
                if i == 'std':
                    count = 2
                temp = 1000*(count)
                for x in j:
                    found_ID[x] += temp
                    temp -= (10*(count))
        return found_ID

    def sel_high_comb(self, ID, comb_features, rr):
        max_ = max(ID)
        index_ = ID.index(max_)
        t = 0
        if len(rr) > 0 and rr[index_] < 0:
            t = -1
        else:
            t = 1
        return (comb_features[index_], t)

