import os
import shutil
from few_shot.pychip_classifier import PychipClassifier
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np


def preprocess_this_directory(base_directory, img_folder, preprocess_folder, specific_img=''):
    data_path = base_directory + img_folder
    if specific_img and isinstance(specific_img, str):
        img_filepath_lst = [specific_img]
    elif specific_img and isinstance(specific_img, list):
        img_filepath_lst = specific_img
    else:
        img_filepath_lst = os.listdir(data_path)

    for img_filename in img_filepath_lst:
        img_name, img_ext = img_name_ext(img_filename)
        preprocessed_path = base_directory + preprocess_folder
        preprocess_image(data_path, img_name, img_ext, preprocessed_path)
    return


def img_name_ext(img_filename):
    img_name_lst = img_filename.split('.')
    img_ext = f".{img_name_lst[-1]}"
    length = len(img_ext)
    img_name = img_filename[:-length]
    return img_name, img_ext


def preprocess_image(data_path, img_name, img_ext, preprocessed_path):
    im1 = PychipClassifier(data_path, img_name + img_ext)

    im1.preprocess("CLAHE", savepath=preprocessed_path, img_name=f"{img_name}{img_ext}", clipLimit=2.0)
    return


if __name__ == '__main__':
    base_directory = '../../datasets/lsfo/'
    img_folder = 'lsfo_originals/'
    preprocess_folder = 'lsfo_clahe/'

    preprocess_this_directory(base_directory, img_folder, preprocess_folder)
