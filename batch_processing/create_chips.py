import os
import shutil
from few_shot.pychip_classifier import PychipClassifier
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np


def chip_this_directory(base_directory, img_folder, chips_folder, n_cols, specific_img=''):
    data_path = base_directory + img_folder
    if specific_img and isinstance(specific_img, str):
        img_filepath_lst = [specific_img]
    elif specific_img and isinstance(specific_img, list):
        img_filepath_lst = specific_img
    else:
        img_filepath_lst = os.listdir(data_path)

    for img_filename in img_filepath_lst:
        img_name, img_ext = img_name_ext(img_filename)
        chips_path = base_directory + chips_folder + img_name + f"_{n_cols}cols"
        create_img_chip_folder(chips_path)
        chip_image(data_path, img_name, img_ext, chips_path, n_cols)
        save_grid_img(data_path, img_filename, n_cols)
    return


def img_name_ext(img_filename):
    img_name_lst = img_filename.split('.')
    img_ext = f".{img_name_lst[-1]}"
    length = len(img_ext)
    img_name = img_filename[:-length]
    return img_name, img_ext


def create_img_chip_folder(chips_path):
    if os.path.isdir(chips_path):
        shutil.rmtree(chips_path)
    os.mkdir(chips_path)
    return


def chip_image(data_path, img_name, img_ext, chips_path, n_cols):
    im1 = PychipClassifier(data_path, img_name + img_ext)

    #im1.preprocess("CLAHE", clipLimit=2.0)
    im1.chips_genesis(n_cols, savepath=chips_path, imgs_ext='.jpg')
    return


def save_grid_img(data_path, img_filename, n_cols):
    img = mpimg.imread(data_path+img_filename)
    width = img.shape[0]
    chipsize = width / n_cols
    x_grid_lst = np.linspace(chipsize, width - chipsize, n_cols - 1)

    plt.figure(figsize=(15, 15))
    plt.axis('off')
    imgplot = plt.imshow(img)
    for x_loc in x_grid_lst:
        plt.axvline(x=x_loc, c='lemonchiffon')
        plt.axhline(y=x_loc, c='lemonchiffon')

    save_path = base_directory + 'grid_overlays/'
    img_name = img_filename.split('.')
    img_name.insert(-1, f'grid{n_cols}')
    img_name = '.'.join(img_name)
    plt.savefig(save_path+img_name)
    return


if __name__ == '__main__':
    # note, requires the "to be chipped" directory to be within the base_directory
    base_directory = '../../datasets/lsfo/'
    img_folder = 'lsfo_originals/'
    chips_folder = 'chipped_images/'
    n_cols = 20

    # these ones like n_cols=33:
    lst_33 = ['0.1dpa-La0.75Sr0.25FeO3-STO-122820-a.tif',
              '0.1dpa-LFO-defect-domains.tif',
              '0.5dpa-La0.5Sr0.5FeO3.tif',
              '0.5dpa-La0.75Sr0.25FeO3-STO-122820-a.tif',
              '0.5dpa-LFO-defect-domains.tif',
              '0.5dpa-LFO-pristine.tif',
              '0.1dpa-LFO-pristine.tif']

    # these ones like n_cols=20:
    lst_20 = ['0.1dpa-La0.5Sr0.5FeO3.tif',
              '0dpa-La0.5Sr0.5FeO3.tif',
              '0dpa-LFO-defect-domains.tif',
              '0dpa-LFO-pristine.tif']

    # these ones like n_cols=30
    lst_30 = ['0dpa-La0.75Sr0.25FeO3-STO-122820-a.tif']

    chip_this_directory(base_directory, img_folder, chips_folder, n_cols, specific_img=lst_20)
