#!/bin/bash
#SBATCH --job-name=jupyter
#SBATCH --time=2-00:00:00
#SBATCH --output=jupyter2.log

module load python/miniconda3.8
source activate /share/apps/python/miniconda3.8
pip install --user notebook
#pip install --user jinja2
#pip install --user prometheus_client
#pip install --user nbformat
#pip install --user send2trash
#pip install --user websocket
#cat /etc/hosts
jupyter notebook --ip=0.0.0.0 --port=8888
